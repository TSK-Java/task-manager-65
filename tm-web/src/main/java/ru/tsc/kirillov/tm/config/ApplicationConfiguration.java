package ru.tsc.kirillov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.tsc.kirillov.tm")
public class ApplicationConfiguration {
}
