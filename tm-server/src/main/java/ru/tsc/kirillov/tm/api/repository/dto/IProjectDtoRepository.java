package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.List;

public interface IProjectDtoRepository extends IWbsDtoRepository<ProjectDto> {

    @NotNull
    @Query("SELECT id FROM ProjectDto WHERE userId = :userId")
    List<String> findAllId(@Nullable @Param("userId") String userId);

}
