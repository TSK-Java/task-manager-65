package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDto;
import ru.tsc.kirillov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    @Nullable
    UserDto removeByLogin(@Nullable String login);

    @Nullable
    UserDto setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    UserDto updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
