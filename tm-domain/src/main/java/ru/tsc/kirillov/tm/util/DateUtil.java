package ru.tsc.kirillov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.exception.system.value.ValueNotValidDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "dd.MM.yyyy";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @NotNull
    static Date toDate(@Nullable final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (@NotNull final ParseException e) {
            @NotNull ValueNotValidDate childException = new ValueNotValidDate(value, PATTERN);
            childException.initCause(e);
            throw childException;
        }
    }

    @NotNull
    static String toString(@Nullable final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
