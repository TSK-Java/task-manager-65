package ru.tsc.kirillov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataXmlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataXmlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
