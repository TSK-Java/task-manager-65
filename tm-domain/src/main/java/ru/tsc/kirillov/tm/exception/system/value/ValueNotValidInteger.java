package ru.tsc.kirillov.tm.exception.system.value;

public final class ValueNotValidInteger extends AbstractValueException {

    public ValueNotValidInteger() {
        super("Ошибка! Значение не является числом.");
    }

    public ValueNotValidInteger(final String value) {
        super(
                String.format("Ошибка! Значение `%s` не является числом.",
                        value == null ? "null" : value
                )
        );
    }

}
