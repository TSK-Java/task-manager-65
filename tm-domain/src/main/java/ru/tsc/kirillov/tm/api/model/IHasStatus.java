package ru.tsc.kirillov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
