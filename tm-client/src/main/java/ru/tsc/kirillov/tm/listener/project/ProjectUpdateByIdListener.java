package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновить проект по его ID.";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Обновление проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken(), id, name, description);
        getProjectEndpoint().updateProjectById(request);
    }

}
