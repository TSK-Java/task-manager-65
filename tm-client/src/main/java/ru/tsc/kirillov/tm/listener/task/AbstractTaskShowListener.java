package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.util.DateUtil;

@Component
public abstract class AbstractTaskShowListener extends AbstractTaskListener {

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("Имя: " + task.getName());
        System.out.println("Описание: " + task.getDescription());
        System.out.println("Статус: " + Status.toName(task.getStatus()));
        System.out.println("Дата создания: " + DateUtil.toString(task.getCreated()));
        System.out.println("Дата начала: " + DateUtil.toString(task.getDateBegin()));
    }

}
